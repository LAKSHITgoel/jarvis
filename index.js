import { ApolloServer } from 'apollo-server';
import schema from './schema';
import resolvers from './resolvers';
import mongoose from 'mongoose';
require('dotenv/config');

mongoose.connect(
  `mongodb://${process.env.DBUSER}:${process.env.DBPASS}@ds243344.mlab.com:43344/jarvis`,
  { useNewUrlParser: true }
);
console.log(process.env.DBUSER, process.env.DBPASS, process.env.PORT);
mongoose.connection.once('open', () => console.log('db connected'));

const server = new ApolloServer({
  typeDefs: schema,
  resolvers
});

server.listen({ port: process.env.PORT }, () =>
  console.log('Server is listening on PORT', process.env.PORT)
);
