const bcrypt = require('bcryptjs');

class Hash {
  static createHash(password) {
    return bcrypt.hashSync(password, 10);
  }
}

export default Hash;
