import mongoose from 'mongoose';

const userSchema = mongoose.Schema({
  fname: { type: String },
  lname: { type: String },
  email: { type: String },
  password: { type: String },
  created_at: { type: String },
  updated_at: { type: String },
  ip_address: { type: String },
  mobile: { type: String }
});

const userModel = mongoose.model('User', userSchema);

// export default userModel;
module.exports = userModel;
