import Hash from '../lib/Hash';
import userModel from '../models/User';

class User {
  createUser(fname, lname, email, password, mobile) {
    try {
      let newUser = new userModel({
        fname,
        lname,
        email,
        password: () => Hash.createHash(password),
        mobile,
        created_at: new Date().toLocaleString()
      });
      return newUser.save();
    } catch (err) {
      console.error(err);
    }
  }

  static getAllUsers() {
    return userModel.find({});
  }
}

export default User;
// module.exports = User;
