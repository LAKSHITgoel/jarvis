import User from './User';

const resolver = {
  Query: {
    getAllUser: () => User.getAllUser()
  },
  Mutation: {
    createUser: (fname, lname, email, password, mobile) => {
      let newuser = new User();
      return newuser.createUser(fname, lname, email, password, mobile);
    }
  }
};

export default resolver;
