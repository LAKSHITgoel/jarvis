import { gql } from 'apollo-server';

const schema = gql`
  type Query {
    getAllUser: [User]!
  }

  type Mutation {
    createUser(
      fname: String!
      lname: String!
      email: String!
      password: String!
      mobile: String!
    ): User
  }

  type User {
    id: String!
    fname: String!
    lname: String!
    email: String!
    password: String!
    mobile: String!
    devices: [Device]
    created_at: String
    updated_at: String
    ip_address: String
  }

  type Device {
    id: String!
    userID: String!
    appliences: [Applience]!
    ip_address: String!
    mac_address: String!
    status: Int
  }

  type Applience {
    id: String!
    deviceID: String!
    userID: String!
    applience_name: String!
    status: Int
  }
`;

//for satus 0:inactive 1:active 2:not_responding

export default schema;
